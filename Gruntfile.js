

module.exports = function ( grunt ) {
  grunt.loadNpmTasks('grunt-loopback-sdk-angular');
  grunt.loadNpmTasks('grunt-docular');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.initConfig({
    loopback_sdk_angular: {
      services: {
        options: {
          input: 'server/server.js',
          output: 'client/js/lb-services.js'
        }
      }
    },
    docular: {
      groups: [
        {
          groupTitle: 'LoopBack',
          groupId: 'loopback',
          sections: [
            {
              id: 'lbServices',
              title: 'LoopBack Services',
              scripts: [ 'client/js/lb-services.js' ]
            }
          ]
        }
      ]
    },
    watch: {
      files: ['server/model-config.json','common/models/**'],
      tasks: ['default']
    }
  });

  grunt.registerTask('default', ['loopback_sdk_angular']);
};
